package com.example.manar.movieapp.rest;

import com.example.manar.movieapp.models.Reviews;
import com.example.manar.movieapp.models.Trailer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by manar on 04/11/17.
 */

public interface ClientInterface {

    @GET("/3/movie/{id}/reviews")
    Call<Reviews> getReviews(@Path("id") int id,
                             @Query("api_key") String key) ;

    @GET("/3/movie/{id}/videos")
    Call<Trailer> getTrailer(@Path("id") int id,
                             @Query("api_key") String key) ;
}