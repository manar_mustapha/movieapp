package com.example.manar.movieapp.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class Database extends SQLiteOpenHelper {
    private static final String NAME = "movie.db";
    private static final int VERSION = 1;

    Database(Context context){
        super(context,NAME,null,VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String CREATE_TABLE = "CREATE TABLE " + Contract.MovieEntry.TABLE_NAME + " (" +
                Contract.MovieEntry._ID + " INTEGER PRIMARY KEY, " +
                Contract.MovieEntry.ID_COLUMN + " INTEGER NOT NULL, " +
                Contract.MovieEntry.IMAGE_COLUMN+ " TEXT NOT NULL," +
                Contract.MovieEntry.TITLE_COLUMN+ " TEXT NOT NULL" +
                ");";
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Contract.MovieEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}