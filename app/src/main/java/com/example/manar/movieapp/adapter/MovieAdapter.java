package com.example.manar.movieapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.manar.movieapp.R;
import com.example.manar.movieapp.controller.NotifyFragment;
import com.example.manar.movieapp.models.Movies;

import java.util.List;

/**
 * Created by manar on 06/10/17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private MovieAdapter.OnClickHandler onClickHandler;
    private List<Movies> moviesList;
    private Context context;
    private boolean firstTimeInit = false;

    public MovieAdapter(List<Movies> moviesList , Context context , MovieAdapter.OnClickHandler onClickHandler) {
        this.onClickHandler = onClickHandler;
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        if (position == moviesList.size() - 1 && !firstTimeInit) {
            firstTimeInit = true;
        }
        holder.setImageView(moviesList.get(position).getPosterPath());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    public interface OnClickHandler {
        void onPhotoClickListener(Movies movies);
    }


    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView moviePoster;

        MovieViewHolder(View itemView) {
            super(itemView);
            moviePoster = itemView.findViewById(R.id.recycle_view_item);
            moviePoster.setOnClickListener(this);
        }

        void setImageView(String posterPath){
            Glide.with(context).load(posterPath).into(moviePoster);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            onClickHandler.onPhotoClickListener(moviesList.get(position));
        }
    }
}
