package com.example.manar.movieapp.sqlite;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;


public class Contract {
    static final String AUTHORITY = "com.example.manar.movieapp";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    static final String PATH_MOVIE = "movie";


    private Contract() {
    }

    public static class MovieEntry implements BaseColumns {
        public static Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOVIE)
                .build();
        static final String TABLE_NAME = "favourite";
        public static final String ID_COLUMN = "id";
        public static final String IMAGE_COLUMN = "image";
        public static final String TITLE_COLUMN = "title";
        public static Uri buildMovieUri(int id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}