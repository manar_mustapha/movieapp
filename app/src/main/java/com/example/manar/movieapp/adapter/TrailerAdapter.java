package com.example.manar.movieapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.manar.movieapp.R;
import com.example.manar.movieapp.models.Trailer;

/**
 * Created by manar on 08/11/17.
 */

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.TrailerViewHolder>  {

    public TrailerAdapter.OnClickHandler onClickHandler;
    Trailer trailer;
    Context context;

    public TrailerAdapter(Trailer trailer, Context context , TrailerAdapter.OnClickHandler onClickHandler ) {
        this.trailer = trailer;
        this.context = context;
        this.onClickHandler = onClickHandler;
    }


    public interface OnClickHandler {
        void onTrailerClicked(String path);

    }
    @Override
    public TrailerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trailer_item, parent, false);
        return new TrailerAdapter.TrailerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrailerViewHolder holder, int position) {
        holder.trailerTxt.setText(trailer.getResults().get(position).getName());
    }

    @Override
    public int getItemCount() {
        return trailer.getResults().size();
    }

    class TrailerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView trailerTxt;

        TrailerViewHolder(View itemView) {
            super(itemView);
            trailerTxt = itemView.findViewById(R.id.trailer);
            trailerTxt.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.trailer){
                onClickHandler.onTrailerClicked(trailer.getResults().get(getAdapterPosition()).getKey());
            }
        }
    }
}