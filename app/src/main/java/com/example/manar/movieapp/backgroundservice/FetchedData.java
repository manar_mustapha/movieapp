package com.example.manar.movieapp.backgroundservice;

import org.json.JSONException;

/**
 * Created by manar on 07/10/17.
 */

public interface FetchedData {
    void onDataFetched(String moviesFromJson) throws JSONException;
}
