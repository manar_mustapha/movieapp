package com.example.manar.movieapp.controller;

import com.example.manar.movieapp.models.Constants;
import com.example.manar.movieapp.models.Reviews;
import com.example.manar.movieapp.rest.ApiCLient;
import com.example.manar.movieapp.rest.ClientInterface;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by manar on 04/11/17.
 */

public class ReviewsController {

    private String api_key = Constants.key;
    private int movieID;

    public ReviewsController(int movieID) {
        this.movieID = movieID;
    }

    public void getReviews(Callback<Reviews> callback){
        ClientInterface apiServiceGetReviews = ApiCLient.getClient().create(ClientInterface.class);
        final Call<Reviews> getReviewsRequest = apiServiceGetReviews.getReviews(movieID, api_key);
        getReviewsRequest.enqueue(callback);
    }
}
