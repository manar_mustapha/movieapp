package com.example.manar.movieapp.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.manar.movieapp.R;
import com.example.manar.movieapp.adapter.TrailerAdapter;
import com.example.manar.movieapp.models.Trailer;

import java.util.zip.Inflater;

/**
 * Created by manar on 08/11/17.
 */

class TrailerDialogue {

    private Activity activity;
    private Trailer trailer;

    TrailerDialogue(Activity activity, Trailer trailer) {
        this.activity = activity;
        this.trailer = trailer;
    }

    void onCreateDialogue() {
        MaterialDialog dialog = new MaterialDialog.Builder(activity)
                .customView(R.layout.trailer_dialogue, false)
                .show();
        View inflater = dialog.getCustomView();
        assert inflater != null;
        RecyclerView recyclerView = inflater.findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        TrailerAdapter trailerAdapter = new TrailerAdapter(trailer, activity, new TrailerAdapter.OnClickHandler() {
            @Override
            public void onTrailerClicked(String path) {
                String trailerUrl = "https://www.youtube.com/watch?v=" + path + "\n\n";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(trailerUrl));
                activity.startActivity(intent);
            }
        });
        recyclerView.setAdapter(trailerAdapter);
    }
}