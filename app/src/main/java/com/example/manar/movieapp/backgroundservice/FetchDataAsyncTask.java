package com.example.manar.movieapp.backgroundservice;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.manar.movieapp.R;
import com.example.manar.movieapp.view.MainActivity;
import com.example.manar.movieapp.view.MainActivityFragment;

import org.json.JSONException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by manar on 06/10/17.
 */

public class FetchDataAsyncTask extends AsyncTask<String, Void, String> {


    private FetchedData fetchedData;
    private Activity activity;

    public FetchDataAsyncTask(Activity activity) {
        this.activity = activity;
    }

    public void setFetchedData(FetchedData fetchedData) {
        this.fetchedData = fetchedData;
    }

    @Override
    protected String doInBackground(String... strings) {
        URL movieRequestUrl;
        if (strings[0].equals("popular")) {
            movieRequestUrl = NetworkUtils.buildUrl("popular");
        } else {
            movieRequestUrl = NetworkUtils.buildUrl("top_rated");
        }
        try {
            return NetworkUtils
                    .getResponseFromHttpUrl(movieRequestUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            if (s != null) {
                fetchedData.onDataFetched(s);
            } else {
                Toast.makeText(activity, R.string.no_internet, Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
