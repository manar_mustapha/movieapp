package com.example.manar.movieapp.controller;

import com.example.manar.movieapp.models.Constants;
import com.example.manar.movieapp.models.Reviews;
import com.example.manar.movieapp.models.Trailer;
import com.example.manar.movieapp.rest.ApiCLient;
import com.example.manar.movieapp.rest.ClientInterface;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by manar on 04/11/17.
 */

public class TrailerController {

    private String api_key = Constants.key;
    private int id ;

    public TrailerController(int id) {
        this.id = id;
    }

    public void getTrailer(Callback<Trailer> callback){
        ClientInterface apiServiceGetTrailer = ApiCLient.getClient().create(ClientInterface.class);
        final Call<Trailer> getTrailerRequest = apiServiceGetTrailer.getTrailer(id, api_key);
        getTrailerRequest.enqueue(callback);
    }
}