package com.example.manar.movieapp.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.manar.movieapp.R;
import com.example.manar.movieapp.sqlite.Contract;

/**
 * Created by manar on 07/11/17.
 */

public class AddAdapter extends RecyclerView.Adapter<AddAdapter.AddedViewHolder> {
    private Cursor mCursor;
    private Context context;

    public AddAdapter(Context context) {
        this.context = context;
    }

    @Override
    public AddedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.added_movie_item, parent, false);
        return new AddedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddedViewHolder holder, int position) {
        mCursor.moveToPosition(position);
        String url = mCursor
                .getString(mCursor.getColumnIndex(Contract.MovieEntry.IMAGE_COLUMN));
        Glide.with(context).load(url).into(holder.courseImage);
    }

    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public Cursor swapCursor(Cursor c) {
        // check if this cursor is the same as the previous cursor (mCursor)
        if (mCursor == c) {
            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        this.mCursor = c; // new cursor value assigned

        //check if this is a valid cursor, then update the cursor
        if (c != null) {
            this.notifyDataSetChanged();
        }
        return temp;
    }

    class AddedViewHolder extends RecyclerView.ViewHolder {
        ImageView courseImage;

        AddedViewHolder(View itemView) {
            super(itemView);
            courseImage = itemView.findViewById(R.id.recycle_view_item);
        }
    }

}

