package com.example.manar.movieapp.controller;

import com.example.manar.movieapp.models.Movies;

import java.util.List;

/**
 * Created by manar on 07/10/17.
 */

public interface Parser {
    void onDataParsing(List<Movies> moviesList);
}
