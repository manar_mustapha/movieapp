package com.example.manar.movieapp.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.os.Bundle;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.manar.movieapp.R;
import com.example.manar.movieapp.adapter.AddAdapter;
import com.example.manar.movieapp.controller.NotifyFragment;
import com.example.manar.movieapp.models.MoviesList;
import com.example.manar.movieapp.sqlite.Contract;
import com.example.manar.movieapp.adapter.MovieAdapter;
import com.example.manar.movieapp.backgroundservice.FetchDataAsyncTask;
import com.example.manar.movieapp.controller.Parser;
import com.example.manar.movieapp.controller.ParserController;
import com.example.manar.movieapp.models.Movies;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements Parser, LoaderManager.LoaderCallbacks<Cursor>{

    private static final int LOADER_ID = 0;
    private static final String MOVIE_LIST_BUNDLE = "movieList";
    FetchDataAsyncTask fetchDataAsyncTask;
    RecyclerView recyclerView;
    MovieAdapter movieAdapter;
    static ProgressBar progressBar;
    ParserController parser;
    Cursor cursor;
    ArrayList<Movies> movieList = new ArrayList<>();
    int position;
    MoviesList parcelableMovieList;
    Bundle savedInstanceStateBundle;
    GridLayoutManager gridLayoutManager;
    int finalposition;

    public MainActivityFragment() {
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        parcelableMovieList = new MoviesList();
        parcelableMovieList.setMoviesList(movieList);
        outState.putParcelable(MOVIE_LIST_BUNDLE, parcelableMovieList);
        outState.putInt("position", finalposition);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        progressBar = view.findViewById(R.id.progress_bar);
        recyclerView = view.findViewById(R.id.movie_recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setSaveEnabled(true);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == recyclerView.SCROLL_STATE_IDLE) {
                    position = ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                }
            }
        });
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns()));
        gridLayoutManager = new GridLayoutManager(getActivity(), numberOfColumns());
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == SCROLL_STATE_IDLE) {
                    Log.v("gridLayoutManager", "gridLayoutManager.findLastVisibleItemPosition(); =----- > " + gridLayoutManager.findLastVisibleItemPosition());
                    finalposition = gridLayoutManager.findLastVisibleItemPosition();
                }
            }
        });


        if (savedInstanceState != null) {
            parcelableMovieList = savedInstanceState.getParcelable(MOVIE_LIST_BUNDLE);
            try {
                savedInstanceStateBundle = savedInstanceState;
                movieList = (ArrayList<Movies>) parcelableMovieList.getMoviesList();
                onDataParsing(movieList);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Refresh the page please", Toast.LENGTH_SHORT).show();
            }
            lastPosition = savedInstanceState.getInt("pI");

        } else {
            String cache = readString();
            if (cache != null) {
                progressBar.setVisibility(View.VISIBLE);
                fetchDataAsyncTask = new FetchDataAsyncTask(getActivity());
                parser = new ParserController();
                parser.setParser(this);
                fetchDataAsyncTask.setFetchedData(parser);
                fetchDataAsyncTask.execute(cache);
            } else {
                progressBar.setVisibility(View.VISIBLE);
                fetchDataAsyncTask = new FetchDataAsyncTask(getActivity());
                parser = new ParserController();
                parser.setParser(this);
                fetchDataAsyncTask.setFetchedData(parser);
                fetchDataAsyncTask.execute("popular");
                getLoaderManager().initLoader(LOADER_ID, null, this);
            }
        }
        return view;
    }

    int lastPosition = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.most_popular) {
            sorting("popular");
            return true;
        }
        if (id == R.id.top_rated) {
            sorting("top_rated");
            return true;
        }

        if (id == R.id.favorite) {
            AddAdapter addAdapter = new AddAdapter(getActivity());
            addAdapter.swapCursor(cursor);
            recyclerView.setAdapter(addAdapter);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void writeString(String property) {
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MOVIE_LIST_BUNDLE, MODE_PRIVATE).edit();
        editor.putString("type", property);
        editor.apply();
    }

    public String readString() {
        return getActivity().getSharedPreferences(MOVIE_LIST_BUNDLE, MODE_PRIVATE).getString("type", null);
    }

    public void sorting(String type) {
        writeString(type);
        fetchDataAsyncTask = new FetchDataAsyncTask(getActivity());
        parser = new ParserController();
        parser.setParser(this);
        fetchDataAsyncTask.setFetchedData(parser);
        fetchDataAsyncTask.execute(type);
    }

    @Override
    public void onDataParsing(List<Movies> moviesList) {
        movieAdapter = new MovieAdapter(moviesList, getActivity(), new MovieAdapter.OnClickHandler() {
            @Override
            public void onPhotoClickListener(Movies movies) {

                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("detail", movies);
                startActivity(intent);
            }
        });
        movieList = (ArrayList<Movies>) moviesList;
        recyclerView.setAdapter(movieAdapter);
        progressBar.setVisibility(View.GONE);

    }

    private int numberOfColumns() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // You can change this divider to adjust the size of the poster
        int widthDivider = 400;
        int width = displayMetrics.widthPixels;
        int nColumns = width / widthDivider;
        if (nColumns < 2) return 2;
        return nColumns;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (savedInstanceStateBundle == null) {
            getLoaderManager().restartLoader(LOADER_ID, null, this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), Contract.MovieEntry.CONTENT_URI,
                new String[]{Contract.MovieEntry._ID,
                        Contract.MovieEntry.ID_COLUMN,
                        Contract.MovieEntry.TITLE_COLUMN,
                        Contract.MovieEntry.IMAGE_COLUMN},
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cursor = data;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

}