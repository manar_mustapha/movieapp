package com.example.manar.movieapp.backgroundservice;

import android.net.Uri;
import android.util.Log;

import com.example.manar.movieapp.models.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by manar on 06/10/17.
 */

class NetworkUtils {

    private static final String TAG = NetworkUtils.class.getSimpleName();

    //MOVIE_BASE_URL = "http://api.themoviedb.org/3/movie/popular?";

    private static final String BASE_URL = "http://api.themoviedb.org/3/movie/";
    private static final String TOP_RATED = "top_rated?";
    private static final String MOST_POPULAR = "popular?";
    private static final String KEY_PARAM = "api_key";
    private static final String KEY = Constants.key;
    /*
     * NOTE: These values only effect responses from OpenWeatherMap, NOT from the fake weather
     * server. They are simply here to allow us to teach you how to build a URL if you were to use
     * a real API.If you want to connect your app to OpenWeatherMap's API, feel free to! However,
     * we are not going to show you how to do so in this course.
     */

    static URL buildUrl(String sort) {
        // COMPLETED (1) Fix this method to return the URL used to query Open Weather Map's API
        Uri builtUri;
        if (sort.equals("popular")) {
            builtUri = Uri.parse(BASE_URL+MOST_POPULAR).buildUpon()
                    .appendQueryParameter(KEY_PARAM, KEY)
                    .build();
        } else {

            builtUri = Uri.parse(BASE_URL+TOP_RATED).buildUpon()
                    .appendQueryParameter(KEY_PARAM, KEY)
                    .build();

        }
        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Log.v(TAG, "Built URI " + url);

        return url;
    }

    /**
     * This method returns the entire result from the HTTP response.
     *
     * @param url The URL to fetch the HTTP response from.
     * @return The contents of the HTTP response.
     * @throws IOException Related to network and stream reading
     */
    static String getResponseFromHttpUrl(URL url) throws IOException {

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}