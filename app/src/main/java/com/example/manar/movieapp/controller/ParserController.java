package com.example.manar.movieapp.controller;

import com.example.manar.movieapp.backgroundservice.FetchedData;
import com.example.manar.movieapp.models.Movies;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manar on 07/10/17.
 */

public class ParserController implements FetchedData{

    private String BASE_POSTER_PATH = "http://image.tmdb.org/t/p/w300";
    private String RELEASE_DATE = "release_date";
    private String POSTER_PATH = "poster_path";
    private String VOTE_AVG = "vote_average";
    private List<Movies> arrayOfParsedData;
    private String OVERVIEW = "overview";
    private String RESULT = "results";
    private String TITLE = "title";
    private String ID = "id";
    private Parser parser;

    public ParserController() {

    }

    public void setParser(Parser parser) {
        this.parser = parser;
    }

    @Override
    public void onDataFetched(String movieResponse) throws JSONException {

        arrayOfParsedData = new ArrayList<>();
        JSONObject movieJson = new JSONObject(movieResponse);
        JSONArray movieArray = movieJson.getJSONArray(RESULT);
        for (int i = 0; i < movieArray.length(); i++) {

            Movies movies = new Movies();
            JSONObject movieItem = movieArray.getJSONObject(i);
            movies.setId(movieItem.getInt(ID));
            movies.setOverview(movieItem.getString(OVERVIEW));
            movies.setPosterPath(BASE_POSTER_PATH+movieItem.getString(POSTER_PATH));
            movies.setReleaseDate(movieItem.getString(RELEASE_DATE));
            movies.setTitle(movieItem.getString(TITLE));
            movies.setVoteAverage(movieItem.getDouble(VOTE_AVG));

            arrayOfParsedData.add(movies);
        }
        parser.onDataParsing(arrayOfParsedData);
    }
}
