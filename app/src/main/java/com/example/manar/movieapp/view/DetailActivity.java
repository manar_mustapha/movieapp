package com.example.manar.movieapp.view;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.manar.movieapp.R;
import com.example.manar.movieapp.sqlite.Contract;
import com.example.manar.movieapp.controller.ReviewsController;
import com.example.manar.movieapp.controller.TrailerController;
import com.example.manar.movieapp.models.Movies;
import com.example.manar.movieapp.models.Reviews;
import com.example.manar.movieapp.models.Trailer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView posterPath;
    TextView title;
    TextView releaseDate;
    TextView voteAvg;
    TextView overView;
    TextView reviews;
    Movies movies;
    Button trailer;
    Button reviewers;
    ImageView favorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        if (intent.hasExtra("detail"))
            movies = intent.getParcelableExtra("detail");

        releaseDate = findViewById(R.id.release_date);
        posterPath = findViewById(R.id.poster_path);
        reviews = findViewById(R.id.reviews_txt);
        overView = findViewById(R.id.over_view);
        reviewers = findViewById(R.id.reviewers);
        voteAvg = findViewById(R.id.vote_avg);
        trailer = findViewById(R.id.trailer);
        title = findViewById(R.id.title);
        favorite = findViewById(R.id.favorite);

        reviewers.setOnClickListener(this);
        trailer.setOnClickListener(this);
        favorite.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(movies.getTitle());

    }

    @Override
    protected void onStart() {
        super.onStart();
        Glide.with(this).load(movies.getPosterPath()).into(posterPath);
        voteAvg.setText(String.valueOf(movies.getVoteAverage()));
        releaseDate.setText(movies.getReleaseDate());
        title.setText(movies.getTitle());
        overView.setText(movies.getOverview());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.reviewers: {
                ReviewsController reviewsController = new ReviewsController(movies.getId());
                reviewsController.getReviews(new Callback<Reviews>() {
                    @Override
                    public void onResponse(Call<Reviews> call, Response<Reviews> response) {
                        if (response != null) {
                            if (response.body() != null) {
                                if (response.body().getTotalResults() > 0) {
                                    for (int i = 0; i < response.body().getTotalResults(); i++) {
                                        reviews.append(response.body().getResults().get(i).getAuthor() + "\n");
                                        reviews.append(response.body().getResults().get(i).getContent() + "\n");
                                    }
                                } else {
                                    Toast.makeText(DetailActivity.this, "This movie has no reviewers", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Reviews> call, Throwable t) {
                        Toast.makeText(DetailActivity.this, "Error happened in reviewers", Toast.LENGTH_LONG).show();
                    }
                });
                break;
            }
            case R.id.trailer: {

                TrailerController trailerController = new TrailerController(movies.getId());
                trailerController.getTrailer(new Callback<Trailer>() {
                    @Override
                    public void onResponse(Call<Trailer> call, Response<Trailer> response) {
                        if (response != null) {
                            if (response.body() != null) {
                                new TrailerDialogue(DetailActivity.this , response.body()).onCreateDialogue();
                            } else {
                                Toast.makeText(DetailActivity.this, R.string.error_trailer, Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Trailer> call, Throwable t) {
                        Toast.makeText(DetailActivity.this, "Error happened in trailer", Toast.LENGTH_LONG).show();
                    }
                });
                break;
            }
            case R.id.favorite: {
                Cursor cursor = getContentResolver()
                        .query(Contract.MovieEntry.CONTENT_URI, new String[]{
                                        Contract.MovieEntry.ID_COLUMN}
                                , Contract.MovieEntry.ID_COLUMN + " = ?"
                                , new String[]{String.valueOf(movies.getId())}
                                , null);

                if (cursor != null && cursor.getCount() > 0) {
                    int delete = getContentResolver().delete(
                            Contract.MovieEntry.buildMovieUri(movies.getId()),
                            Contract.MovieEntry.ID_COLUMN + " = ?",
                            new String[]{String.valueOf(movies.getId())}
                    );
                    if (delete > 0) {
                        favorite.setImageDrawable(getDrawable(R.drawable.ic_favorite_border_black_24dp));
                        Toast.makeText(this, R.string.movie_deleted, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //add movie with title
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Contract.MovieEntry.ID_COLUMN, movies.getId());
                    contentValues.put(Contract.MovieEntry.TITLE_COLUMN, movies.getTitle());
                    contentValues.put(Contract.MovieEntry.IMAGE_COLUMN, movies.getPosterPath());
                    getContentResolver()
                            .insert(Contract.MovieEntry.CONTENT_URI, contentValues);
                    favorite.setImageDrawable(getDrawable(R.drawable.ic_favorite_black_24dp));
                    Toast.makeText(this, R.string.movie_added, Toast.LENGTH_SHORT).show();
                }
                cursor.close();
                break;
            }
        }
    }
}